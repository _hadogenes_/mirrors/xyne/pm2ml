#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''pm2ml''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1627930500)),
  description='''Generate metalinks for downloading Pacman packages and databases.''',
  author='''Xyne''',
  author_email='''ac xunilhcra enyx, backwards''',
  url='''http://xyne.archlinux.ca/projects/pm2ml''',
  py_modules=['''pm2ml'''],
)
